"""Stream type classes for tap-storefeeder."""

from socketserver import ThreadingUnixStreamServer
from typing import Any, Dict, Optional

from singer_sdk import typing as th

from tap_storefeeder.client import StoreFeederStream


class OrdersStream(StoreFeederStream):
    """Define the Orders Stream."""

    name = "orders"
    path = "/orders"
    primary_keys = ["OrderNumber", "OrderGuid"]
    replication_key = "LastStatusChangeDate"
    filter_date = "LastStatusChangeDateFrom"
    records_jsonpath = "$.Data[*]"
    schema = th.PropertiesList(
        th.Property("OrderNumber", th.IntegerType),
        th.Property("OrderGuid", th.StringType),
        th.Property("OrderStatus", th.StringType),
        th.Property(
            "Warehouse",
            th.ObjectType(
                th.Property("WarehouseName", th.StringType),
                th.Property("WarehouseGuid", th.StringType),
                th.Property("WarehouseID", th.IntegerType),
            ),
        ),
        th.Property(
            "Channel",
            th.ObjectType(
                th.Property("ChannelName", th.StringType),
                th.Property("ChannelType", th.StringType),
                th.Property("ChannelID", th.IntegerType),
                th.Property("ChannelGuid", th.StringType),
                th.Property("IsActive", th.BooleanType),
            ),
        ),
        th.Property("ChannelOrderRef", th.StringType),
        th.Property(
            "OrderTotals",
            th.ObjectType(
                th.Property("Subtotal", th.NumberType),
                th.Property("Tax", th.NumberType),
                th.Property("ShippingCost", th.NumberType),
                th.Property("Discount", th.NumberType),
                th.Property("Total", th.NumberType),
                th.Property("ShippingNet", th.NumberType),
                th.Property("ShippingTax", th.NumberType),
            ),
        ),
        th.Property(
            "Customer",
            th.ObjectType(
                th.Property("CustomerID", th.IntegerType),
                th.Property("CustomerGuid", th.StringType),
            ),
        ),
        th.Property(
            "BillingAddress",
            th.ObjectType(
                th.Property("FirstName", th.StringType),
                th.Property("LastName", th.StringType),
                th.Property("Company", th.StringType),
                th.Property("Address1", th.StringType),
                th.Property("Address2", th.StringType),
                th.Property("Suite", th.StringType),
                th.Property("City", th.StringType),
                th.Property("State", th.StringType),
                th.Property("Postcode", th.StringType),
                th.Property("Country", th.StringType),
                th.Property("PhoneNumber", th.StringType),
            ),
        ),
        th.Property(
            "Shippingaddress",
            th.ObjectType(
                th.Property("FirstName", th.StringType),
                th.Property("LastName", th.StringType),
                th.Property("Company", th.StringType),
                th.Property("Address1", th.StringType),
                th.Property("Address2", th.StringType),
                th.Property("Suite", th.StringType),
                th.Property("City", th.StringType),
                th.Property("State", th.StringType),
                th.Property("Postcode", th.StringType),
                th.Property("Country", th.StringType),
                th.Property("PhoneNumber", th.StringType),
            ),
        ),
        th.Property("OrderDate", th.DateTimeType),
        th.Property("AttentionIsRequired", th.BooleanType),
        th.Property("IsPrime", th.BooleanType),
        th.Property("IsPriority", th.BooleanType),
        th.Property("ImportDate", th.DateTimeType),
        th.Property("DespatchDate", th.DateTimeType),
        th.Property("DespatchSentToChannelDate", th.DateTimeType),
        th.Property("DespatchOnChanelDate", th.DateTimeType),
        th.Property("LastStatusChangeDate", th.DateTimeType),
        th.Property("PaymentMethod", th.StringType),
        th.Property("PaymentID", th.StringType),
        th.Property("ChannelShippingMethod", th.StringType),
        th.Property(
            "ShippingMethod",
            th.ObjectType(
                th.Property("ShippingMethodID", th.IntegerType),
                th.Property("Name", th.StringType),
                th.Property("ServiceCode", th.StringType),
            ),
        ),
        th.Property("ShippingTrackingNumber", th.StringType),
        th.Property("ShippedVia", th.StringType),
        th.Property("ConsignmentNumber", th.StringType),
        th.Property("Weight", th.NumberType),
        th.Property("NumberOfPackages", th.IntegerType),
        th.Property(
            "Packaging",
            th.ObjectType(
                th.Property("PackagingID", th.IntegerType),
                th.Property("PackagingName", th.StringType),
            ),
        ),
        th.Property("PickerName", th.StringType),
        th.Property("PackerName", th.StringType),
        th.Property("PickwaveID", th.IntegerType),
        th.Property("PickerInstructions", th.StringType),
        th.Property("SpecialInstructions", th.StringType),
        th.Property(
            "ContactNotes",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ContactNoteID", th.IntegerType),
                    th.Property("Subject", th.StringType),
                    th.Property("Body", th.StringType),
                    th.Property("Date", th.DateTimeType),
                    th.Property("ContactType", th.IntegerType),
                )
            ),
        ),
        th.Property("Currency", th.StringType),
        th.Property("ExchangeRate", th.NumberType),
        th.Property(
            "Transactions",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TransactionID", th.IntegerType),
                    th.Property("TransactionRef", th.StringType),
                    th.Property("Subtotal", th.NumberType),
                    th.Property("Discount", th.NumberType),
                    th.Property("Tax", th.NumberType),
                    th.Property("ShippingCost", th.NumberType),
                    th.Property("TransactionDate", th.DateTimeType),
                    th.Property("TransactionMethod", th.StringType),
                )
            ),
        ),
        th.Property(
            "OrderLines",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "Product",
                        th.ObjectType(
                            th.Property("ProductID", th.IntegerType),
                            th.Property("SKU", th.StringType),
                        ),
                    ),
                    th.Property("OrderedProductSKU", th.StringType),
                    th.Property("ChannelSKU", th.StringType),
                    th.Property("OrderedProductName", th.StringType),
                    th.Property("QuantityOrdered", th.IntegerType),
                    th.Property("QuantitySent", th.IntegerType),
                    th.Property("QuantityReturned", th.IntegerType),
                    th.Property("LineStatus", th.StringType),
                    th.Property("SingleItemPrice", th.NumberType),
                    th.Property("SingleItemPriceExVat", th.NumberType),
                    th.Property("TotalItemPriceIncVat", th.NumberType),
                    th.Property("VatTotal", th.NumberType),
                    th.Property("RegularPrice", th.NumberType),
                    th.Property("TaxRate", th.NumberType),
                    th.Property("Discount", th.NumberType),
                    th.Property("ScannedProductBarcode", th.StringType),
                    th.Property("IsComponent", th.BooleanType),
                    th.Property("IsKit", th.BooleanType),
                    th.Property("TextOption", th.StringType),
                )
            ),
        ),
        th.Property(
            "CompanyIdentity",
            th.ObjectType(
                th.Property("CompoanyIdentityID", th.NumberType),
                th.Property("CompanyIdentityName", th.StringType),
            ),
        ),
    ).to_dict()


class ProductsStream(StoreFeederStream):
    """Define the Orders Stream."""

    name = "products"
    path = "/products"
    primary_keys = ["ProductID", "SKU"]
    replication_key = "LastModifiedDate"
    filter_date = "ModifiedFrom"
    records_jsonpath = "$.Data[*]"
    schema = th.PropertiesList(
        th.Property("ProductID", th.IntegerType),
        th.Property("SKU", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("ProductGUID", th.StringType),
        th.Property("ProductType", th.StringType),
        th.Property(
            "Suppliers",
            th.ArrayType(
                th.ObjectType(
                    th.Property(
                        "Supplier",
                        th.ObjectType(
                            th.Property(
                                "Currency",
                                th.ObjectType(
                                    th.Property("CurrencyID", th.IntegerType),
                                    th.Property("CurrencyCode", th.StringType),
                                    th.Property("CurrencyName", th.StringType),
                                    th.Property("Symbol", th.StringType),
                                ),
                            ),
                            th.Property("SupplierID", th.IntegerType),
                            th.Property("Name", th.StringType),
                        ),
                    ),
                    th.Property("SupplierSKU", th.StringType),
                    th.Property("SupplierCosts", th.NumberType),
                    th.Property("SupplierStockLevel", th.IntegerType),
                    th.Property("SupplierLeadTime", th.IntegerType),
                    th.Property("SupplierCartonQuantities", th.IntegerType),
                    th.Property("Prioritys", th.IntegerType),
                    th.Property("SupplierMinOrderAmount", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "WarehouseInformation",
            th.ObjectType(
                th.Property(
                    "Warehouse",
                    th.ObjectType(
                        th.Property("WarehouseName", th.StringType),
                        th.Property("WarehouseGuid", th.StringType),
                        th.Property("WarehouseID", th.IntegerType),
                    ),
                ),
                th.Property("ContainsHazardousMaterials", th.BooleanType),
                th.Property("IsFragile", th.BooleanType),
                th.Property("UnitOfMesure", th.StringType),
                th.Property("Weight", th.NumberType),
                th.Property(
                    "Packaging",
                    th.ObjectType(
                        th.Property("PackagingID", th.IntegerType),
                        th.Property("PackagingName", th.StringType),
                    ),
                ),
                th.Property("PackagingQuantity", th.IntegerType),
                th.Property("PackagingQuantityDecimal", th.NumberType),
                th.Property("ProductHarmonizedCode", th.StringType),
                th.Property("RecordSerialNumberOnBarcodeDespatch", th.BooleanType),
            ),
        ),
        th.Property(
            "PricingInformation",
            th.ObjectType(
                th.Property(
                    "TaxClass",
                    th.ObjectType(
                        th.Property("TaxClassID", th.IntegerType),
                        th.Property("TaxClassGuid", th.StringType),
                        th.Property("CreatedOn", th.DateTimeType),
                        th.Property("IsGlobal", th.BooleanType),
                        th.Property(
                            "TaxRateCountries",
                            th.ArrayType(
                                th.ObjectType(
                                    th.Property("TaxRateCountryID", th.IntegerType),
                                    th.Property("CountryName", th.StringType),
                                    th.Property(
                                        "ISOCountryCodeTwoLetter", th.StringType
                                    ),
                                    th.Property("TaxRate", th.NumberType),
                                )
                            ),
                            th.Property("Name", th.StringType),
                            th.Property("TaxRate", th.NumberType),
                        ),
                    ),
                ),
                th.Property("Price", th.NumberType),
                th.Property("Margin", th.NumberType),
                th.Property("AverageCost", th.NumberType),
                th.Property("AverageCostExVat", th.NumberType),
                th.Property("PriceIncludesVat", th.BooleanType),
                th.Property("LastCostPrice", th.NumberType),
                th.Property("LastCostPriceExVat", th.NumberType),
            ),
        ),
        th.Property(
            "InventoryInformation",
            th.ObjectType(
                th.Property("Inventory", th.IntegerType),
                th.Property("Allocated", th.IntegerType),
                th.Property("OnBackOrder", th.IntegerType),
                th.Property("OnPurchaseOrder", th.IntegerType),
                th.Property("SoldLast30Days", th.IntegerType),
                th.Property("SoldLast90Days", th.IntegerType),
                th.Property("SoldLast120Days", th.IntegerType),
            ),
        ),
        th.Property("Categories",th.ArrayType(
            th.ObjectType(
                th.Property("CategoryID",th.IntegerType),
                th.Property("CategoryName",th.StringType),
            )
        )),
        th.Property("Brand", th.StringType),
        th.Property("Description", th.StringType),
        th.Property("Archived", th.BooleanType),
        th.Property("IsAkit", th.BooleanType),
        th.Property("CreatedOnDate", th.DateTimeType),
        th.Property("LastModifiedDate", th.DateTimeType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}

        params["IncludeSuppliers"] = True
        params["IncludeCategories"] = True 

        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            start_date = self.get_starting_timestamp(context)
            date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
            params[self.filter_date] = date

        return params


class WarehousesStream(StoreFeederStream):
    """Define the Orders Stream."""

    name = "warehouses"
    path = "/warehouses"
    primary_keys = ["ShippingAddress", "BillingAddress"]
    records_jsonpath = "$.[*]"
    schema = th.PropertiesList(
        th.Property(
            "ShippingAddress",
            th.ObjectType(
                th.Property("Address1", th.StringType),
                th.Property("Address2", th.StringType),
                th.Property("Address3", th.StringType),
                th.Property("City", th.StringType),
                th.Property("State", th.StringType),
                th.Property("Postocode", th.StringType),
                th.Property("Country", th.StringType),
            ),
        ),
        th.Property(
            "BillingAddress",
            th.ObjectType(
                th.Property("Address1", th.StringType),
                th.Property("Address2", th.StringType),
                th.Property("Address3", th.StringType),
                th.Property("City", th.StringType),
                th.Property("State", th.StringType),
                th.Property("Postocode", th.StringType),
                th.Property("Country", th.StringType),
            ),
        ),
        th.Property("IsDefault", th.BooleanType),
        th.Property("MetaPackWarehouseCode", th.StringType),
        th.Property("SenderName", th.StringType),
        th.Property("HandlingTime", th.IntegerType),
        th.Property("MaxOrderValue", th.NumberType),
        th.Property("MaxOrderSubtotal", th.NumberType),
        th.Property("MaxOrderTotal", th.NumberType),
        th.Property("ApplyTaxToShipping", th.BooleanType),
        th.Property("WarehouseType", th.StringType),
        th.Property("WarehouseName", th.StringType),
        th.Property("WarehouseGuid", th.StringType),
        th.Property("WarehouseID", th.IntegerType),
    ).to_dict()

class PurchaseOrdersStream(StoreFeederStream):
    """Define the purchase-orders stream"""

    name = "purchase-orders"
    path = "/purchase-orders"
    primary_keys = ["PurchaseOrderID"]
    records_jsonpath = "$.Data[*]"
    schema = th.PropertiesList(
        th.Property("PurchaseOrderID",th.IntegerType),
        th.Property("PurchaseOrderGuid", th.StringType),
        th.Property("User",th.ObjectType(
                th.Property("UserID",th.IntegerType),
                th.Property("UserName",th.StringType),
            )
        ),
        th.Property("PurchaseOrderReference",th.StringType),
        th.Property("DateRaised",th.DateTimeType),
        th.Property("Supplier",th.ObjectType(
                th.Property("SupplierCode",th.StringType),
                th.Property("SuppliersReference",th.DateTimeType),
                th.Property("Currency",th.ObjectType(
                        th.Property("CurrencyID",th.IntegerType),
                        th.Property("CurrencyCode",th.StringType),
                        th.Property("CurrencyName",th.StringType),
                        th.Property("Symbol",th.StringType),
                    )
                ),
                th.Property("SupplierID",th.IntegerType),
                th.Property("Name",th.StringType),
            )
        ),
        th.Property("Warehouse",th.ObjectType(
                th.Property("WarehouseName",th.StringType),
                th.Property("WarehouseGuid",th.StringType),
                th.Property("WarehouseID",th.IntegerType),
            )
        ),
        th.Property("DateDue",th.DateTimeType),
        th.Property("PurchaseOrderSatatus",th.StringType),
        th.Property("DeliveryCost",th.NumberType),
        th.Property("Subtotal",th.NumberType),
        th.Property("TotalVat",th.NumberType),
        th.Property("Total",th.NumberType),
        th.Property("Curency",th.ObjectType(
                th.Property("CurrencyID",th.IntegerType),
                th.Property("CurrencyCode",th.StringType),
                th.Property("CurrencyName",th.StringType),
                th.Property("Symbol",th.StringType),
            )
        ),
        th.Property("DateSent",th.DateTimeType),
        th.Property("PurchaseOrderLines",th.ArrayType(
                th.ObjectType(
                    th.Property("PurchaseOrderLineID",th.IntegerType),
                    th.Property("ProductSKU",th.StringType),
                    th.Property("QtyOrdered",th.IntegerType),
                    th.Property("QtyReceived",th.IntegerType),
                    th.Property("PurchaseOrderDetailsStatus",th.StringType),
                    th.Property("SinglePrice",th.NumberType),
                    th.Property("TaxRate",th.NumberType),
                    th.Property("LineVat",th.NumberType),
                    th.Property("LineTotal",th.NumberType),
                    th.Property("DelivertStatusID",th.IntegerType),
                    th.Property("DeliveryStatus",th.StringType),
                    th.Property("QtyWrittenOff",th.NumberType),
                    th.Property("CartonQty",th.IntegerType),
                    th.Property("AddedByUserID",th.IntegerType),
                    th.Property("BookedInByUserID",th.IntegerType),
                    th.Property("TotalUnitsOrdered",th.IntegerType),
                    th.Property("TotalUnitsWrittenOff",th.IntegerType),
                    th.Property("LineNotes",th.StringType),
                )
            )
        ),
    ).to_dict()