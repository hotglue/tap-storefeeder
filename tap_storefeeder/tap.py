"""StoreFeeder tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_storefeeder.streams import OrdersStream, ProductsStream, WarehousesStream, PurchaseOrdersStream

STREAM_TYPES = [OrdersStream, WarehousesStream, ProductsStream, PurchaseOrdersStream]


class TapStoreFeeder(Tap):
    """StoreFeeder tap class."""

    name = "tap-storefeeder"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property("user_name", th.StringType, required=True),
        th.Property("client_id", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
        th.Property("start_date", th.DateTimeType, required=False),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapStoreFeeder.cli()
