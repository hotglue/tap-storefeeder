"""REST client handling, including StoreFeederStream base class."""

import logging
from typing import Any, Dict, Optional

import requests
from singer_sdk.streams import RESTStream

from tap_storefeeder.auth import OAuth2Authenticator

logging.getLogger("backoff").setLevel(logging.CRITICAL)

class StoreFeederStream(RESTStream):
    """StoreFeeder stream class."""

    extra_retry_statuses = [429, 409]

    @property
    def url_base(self) -> str:
        return "https://rest.storefeeder.com"

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        url = f"{self.url_base}/Token"
        return OAuth2Authenticator(self, self._tap.config, url)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        try:
            tp = response.json().get("TotalPages", [])
        except:
            self.logger.info(
                f"No 'TotalPages' found on stream {self.tap_stream_id}. Skipping pagination."
            )
            return None
        total_pages = int(tp)
        current_page = int(response.json().get("PagingInfo")["Page"])
        if current_page >= total_pages:
            return None
        return current_page + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        #params[""]
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            start_date = self.get_starting_timestamp(context)
            date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
            params[self.filter_date] = date

        return params

    def backoff_handler(self, details: dict) -> None:
        """Adds additional behaviour prior to retry."""
        logging.info(
            "Backing off {wait:0.1f} seconds after {tries} tries".format(**details)
        )